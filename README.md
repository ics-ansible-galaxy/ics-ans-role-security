# ics-ans-role-security

Ansible role to apply security fixes.
It currently:

- Install all updates that have been marked security related (yum)

## Requirements

- ansible >= 2.4
- molecule >= 2.6

## Role Variables

The variable ``security_packages_to_update`` can restrict the packages to update if defined.

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-security
```

## License

BSD 2-clause
